import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
*
* A stateless object that contains static methods assisting Server.java
*
*/
public class ServerHelper {

	final static ReentrantLock lock = new ReentrantLock();

	public static String browse() {

		String contents = read();
		contents = strip(contents);
		
		if(contents.isEmpty())
			return "Database is empty";
		else {
			StringBuilder catlog = new StringBuilder();
			Map<String,String> fileMap = getMap(contents);
			int row = 1;
			for (Map.Entry<String, String> e : fileMap.entrySet()) {
				    String name = e.getKey().split(Constant.RE_COMMA)[0];
				    String type = e.getKey().split(Constant.RE_COMMA)[1];
				    catlog.append(row++ + ". Name: " + name + ", Type: " + type + "@");
			}

			return catlog.toString();
		}
	}

	public static String del(String client) {

		String name = client.split(Constant.SPACE)[1];
		String type = client.split(Constant.SPACE)[2];
		String key = name + "," + type;
		
		String contents = read();
		contents = strip(contents);

		Map<String,String> fileMap = getMap(contents);
		String value = fileMap.remove(key);

		if(value == null)
			value = "Record was not found";
		else
			write(fileMap);

		return value;
	}

	public static String get(String client) {

		String name = client.split(Constant.SPACE)[1];
		String type = client.split(Constant.SPACE)[2];
		String key = name + "," + type;
		
		String contents = read();
		contents = strip(contents);

		Map<String,String> fileMap = getMap(contents);
		String value = fileMap.get(key);

		if(value == null)
			value = "Record was not found";

		return value;
	}

	public static String put(String client) {

		String entry = createEntry(client.split(Constant.SPACE));
		String contents = read();
		write(entry, contents);
		
		return "you did a put!";
	}

	public static void write(Map<String,String> m) {

        String fileName = Constant.DB;

        try {
        	lock.lock();
            FileWriter fileWriter = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			for (Map.Entry<String, String> e : m.entrySet()) {
			    String name = e.getKey().split(Constant.RE_COMMA)[0];
			    String type = e.getKey().split(Constant.RE_COMMA)[1];
			    String value = e.getValue();
			    bufferedWriter.write(createEntry(name,value,type) + Constant.NL);
			}
            bufferedWriter.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
		    // forget this and you're screwed
		    lock.unlock();
		}
	}

	public static void write(String entry, String contents) {

        String fileName = Constant.DB;

        try {
        	lock.lock();
            FileWriter fileWriter = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            if(!contents.isEmpty()) {

            	contents = strip(contents);
				Map<String,String> fileMap = getMap(contents);

				// REMOVE THIS
				if(!entry.isEmpty()) {
					entry = entry.replaceAll("(\\<|\\>)", "");
					String a[] = entry.split(Constant.RE_COMMA);
					fileMap = updateMap(fileMap, a);
				}

				for (Map.Entry<String, String> e : fileMap.entrySet()) {
				    String name = e.getKey().split(Constant.RE_COMMA)[0];
				    String type = e.getKey().split(Constant.RE_COMMA)[1];
				    String value = e.getValue();
				    bufferedWriter.write(createEntry(name,value,type) + Constant.NL);
				}
			}
			else {
				bufferedWriter.write(entry);
			}

            bufferedWriter.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
		    // forget this and you're screwed
		    lock.unlock();
		}

	}

	public static String read() {

        String fileName = Constant.DB;
        String row = ""; // Refer to string as row like a proper DB
        StringBuilder result = new StringBuilder();
        
        try {

	        FileReader fileReader = new FileReader(fileName);
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        
	        while((row = bufferedReader.readLine()) != null) {
	            System.out.println(row);
	            result.append(row + Constant.NL);
	        }   

	        bufferedReader.close();         
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();              
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
        	return result.toString();
        }
	}

	public static Map<String,String> updateMap(Map<String,String> m, String row[]) {
		String name = row[0];
		String value = row[1];
		String type = row[2];
		String key = name + Constant.COMMA + type;
		m.put(key, value);
		return m;
	}

	public static Map<String,String> getMap(String contents) {

		Map<String,String> fileMap = new HashMap<>(); // Update key to a wrapper class
		String[] entries = contents.split(Constant.SPACE);

		for(String entr: entries) {
			String split[] = entr.split(Constant.RE_COMMA);
			String name = split[0];
			String value = split[1];
			String type = split[2];
			String key = name + Constant.COMMA + type;
			fileMap.put(key, value);
		}

		return fileMap;
	}

	public static String strip(String s) {
		s = s.replaceAll("(\\r|\\n)", "");
		s = s.replaceAll("(\\>\\<)", " ");
		s = s.replaceAll("(\\<)", "");
		s = s.replaceAll("(\\>)", "");
		return s;
	}

	public static void print(Object o) {
		System.out.println(o);
	}

	public static String help() {
		return Constant.MENU + "\n";
	}

	public static String createEntry(String args[]) {
		return Constant.L_BRAK + args[1] + Constant.COMMA + args[2] + Constant.COMMA + args[3] + Constant.R_BRAK;
	}
	public static String createEntry(String n, String v, String t) {
		return Constant.L_BRAK + n + Constant.COMMA + v + Constant.COMMA + t + Constant.R_BRAK;
	}
}