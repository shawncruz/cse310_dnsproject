import java.io.*;
import java.net.*;
import java.util.*;

/**
* Sandbox: this class is for testing
*/
class Sandbox {

	public static void main(String argv[]) throws Exception {
		print(Constant.MENU);
	}

	private static void print(Object s) {
		System.out.println(s);
	}

	private enum Options {
		HELP, PUT, GET, DEL, BROWSE, EXIT
	}
}