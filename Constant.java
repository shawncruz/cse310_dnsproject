class Constant {
	public static final String ERR = "error";
	public static final String DB = "db.txt";
	public static final String L_BRAK = "<";
	public static final String R_BRAK = ">";
	public static final String COMMA = ",";
	public static final String NL = "\n";
	public static final String BORDER = NL + NL;
	public static final String SPACE = "\\s";
	public static final String RE_COMMA = "\\,";
	public static final String MENU = "> help:" + NL
									 + "> put:" + NL
									 + "> get:" + NL
									 + "> del:" + NL
									 + "> browse:" + NL
									 + "> exit:" + NL;
	public static final String SERVER_START = "Server running" + NL
											+ "Waiting for connection...";
	public static final String WELCOME = BORDER + " ####################" + NL + "~#``WELCOME TO DN$``#~" + NL + " ####################";
	public static final String PROMPT_USER = "Enter a command:";
	public static final String PORT = BORDER + "Port Number: ";
}