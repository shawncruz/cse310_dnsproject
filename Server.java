import java.io.*;
import java.net.*;
import java.util.*;

/**
* For Linux machine:
*   Login: shcruz
*   Password: zsFE15CS
*/
public class Server implements Runnable {

   Socket socket;

   Server(Socket socket) {
      this.socket = socket;
   }

  public static void main(String args[]) throws Exception {

      ServerSocket welcomeSocket = new ServerSocket(0);
      // Print ephemeral port to standard output
      print(Constant.PORT + welcomeSocket.getLocalPort());
      print(Constant.SERVER_START);
      System.out.println("Listening");
      
      while (true) {
         Socket sock = welcomeSocket.accept();
         System.out.println("Connected");
         new Thread(new Server(sock)).start();
      }
  }

  public void run() {

      String line="",log="";

      try {

        BufferedReader fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        DataOutputStream toClient = new DataOutputStream(socket.getOutputStream());
        
        while((line = fromClient.readLine()) != null) {
          
            String command = line.split(Constant.SPACE)[0].toUpperCase();
            print("Client sent: " + line);
            print("Command: " + command + Constant.NL);

            String resultString = "";

            // Append to string to log client history
            log = log + line;

            switch(command) {

                case "@START":  resultString = "~WELCOME TO DNS APP!";
                                break;

                case "HELP":    resultString = "HELP MENU SERVER!";
                                break;

                case "PUT":     resultString = ServerHelper.put(line);
                                break;

                case "GET":     resultString = ServerHelper.get(line);
                                break;

                case "DEL":     resultString = ServerHelper.del(line);
                                break;

                case "BROWSE":  resultString = ServerHelper.browse();
                                break;

                case "EXIT":    resultString = line;
                                break;

                default:        resultString = "INVALID COMMAND SERVER!";
                                break;
            }

            toClient.writeBytes(resultString + "\n");
            print("SERVER->CLIENT: " + resultString);

        }

        // Display log history of client
        print("Log:" + log);
        
        socket.close();
        toClient.close();
      }
      catch (IOException e) {
         System.out.println(e);
      }
   }

  private static void print(Object o) {
      System.out.println(o);
  }

}