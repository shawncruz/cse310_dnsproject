import java.io.*;
import java.net.*;

class TCPClient {

	public static void main(String argv[]) throws Exception {

		String inputString = "", serverString = "";
		/**
		* GET:
		* (i) the host name of the machine on which the server program is running
	 	*	example: "0.0.0.0"(localhost), "130.245.148.164"
		* (ii) the port number that the server is listening at; unknown until server app starts
		*/
		String hostName = argv[0];
		int portNumber = Integer.parseInt(argv[1]);

		BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
		
		print(Constant.WELCOME);

		while(true) {

			// Create client socket; connect to server
			Socket clientSocket = new Socket(hostName, portNumber);
			// Create output stream attached to client socket
			DataOutputStream toServer = new DataOutputStream(clientSocket.getOutputStream());
			// Create input stream attached to client socket
			BufferedReader fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			// Prompt user for input
			print(Constant.PROMPT_USER);
			inputString = userInput.readLine();
			// Split input string to account for variable length args
			String[] args = inputString.split(Constant.SPACE);
			String command = args[0].toUpperCase();
			Options opt = Options.valueOf(command);

			switch(opt) {

				case HELP:		toServer.writeBytes(command + Constant.NL);
								break;

				case PUT:		toServer.writeBytes(inputString + Constant.NL);
						 		break;

				case GET:		toServer.writeBytes(inputString + Constant.NL);
								break;

				case DEL:		toServer.writeBytes(inputString + Constant.NL);
								break;

				case BROWSE:	toServer.writeBytes(command + Constant.NL);
								break;

				case EXIT:		toServer.writeBytes(command + Constant.NL);
								exit(clientSocket, toServer, fromServer);	
								break;

				default:		toServer.writeBytes(Constant.ERR + Constant.NL);
								print("error: invalid command");
								break;
			}
			StringBuilder output = new StringBuilder();
			while((serverString = fromServer.readLine()) != null) {
	            if (serverString.isEmpty())
        			break;
	            output.append(serverString + Constant.NL);
	        }  
			print("\nFROM SERVER: \n"  + output.toString() + Constant.NL);
		}
		
	}

	public static void exit(Socket s, DataOutputStream d, BufferedReader b) throws Exception {
		b.close();
		d.close();
		s.close();
		System.exit(1);	
	}

	public static void print(Object o) {
		System.out.println(o);
	}

	public enum Options {
		HELP, PUT, GET, DEL, BROWSE, EXIT
	}
}


