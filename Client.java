import java.io.*;
import java.net.*;

public class Client {

    public static void main(String[] args) throws Exception {

        Socket clientSocket = null;

        //DataInputStream is = null;
        BufferedReader fromServer = null;

        //PrintStream os = null;
        DataOutputStream toServer = null;

        //DataInputStream inputLine = null;
        BufferedReader userInput = null;

        String inputString = "", serverString = "";

        try {
            String hostName = args[0];
            int portNumber = Integer.parseInt(args[1]);

            clientSocket = new Socket(hostName, portNumber);

            //os = new PrintStream(clientSocket.getOutputStream());
            toServer = new DataOutputStream(clientSocket.getOutputStream());

            //is = new DataInputStream(clientSocket.getInputStream());
            fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            //inputLine = new DataInputStream(new BufferedInputStream(System.in));
            userInput = new BufferedReader(new InputStreamReader(System.in));

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (clientSocket != null && toServer != null && fromServer != null) {

            try {

                /*
                * Keep on reading from/to the socket till we receive the "exit" from the
                * server, once we received that then we break.
                */
                String responseLine;
                System.out.println("The client started. Type any text. To quit it type 'exit'.");
          
                // Prompt user for input
                print(Constant.PROMPT_USER);

                // TAKE THIS OUT WHEN WORKING ON SERVER SIDE FILE
                toServer.writeBytes("@START" + "\n");

                while ((responseLine = fromServer.readLine()) != null 
                  && !responseLine.equalsIgnoreCase("EXIT")) {

                    // GET RID OF > CHARACTER; MAKE MORE READABLE
                    print(">" + responseLine.replaceAll("\\@", "\n") + "\n");

                    inputString = userInput.readLine();

                    String[] arguments = inputString.split(Constant.SPACE);
                    String command = arguments[0].toUpperCase();
                  
                    switch(command) {

                      case "HELP":    print("HELP CASE!");
                                      print("\n" + Constant.MENU);
                                      toServer.writeBytes(command + Constant.NL);
                                      break;

                      case "PUT":     print("PUT CASE!");
                                      toServer.writeBytes(inputString + Constant.NL);
                                      break;

                      case "GET":     print("GET CASE!");
                                      toServer.writeBytes(inputString + Constant.NL);
                                      break;

                      case "DEL":     print("DEL CASE!");
                                      toServer.writeBytes(inputString + Constant.NL);
                                      break;

                      case "BROWSE":  print("BROWSE CASE!");
                                      toServer.writeBytes(command + Constant.NL);
                                      break;

                      case "EXIT":    print("EXIT CASE!");
                                      toServer.writeBytes(command + Constant.NL);
                                      continue;

                      default:        toServer.writeBytes(command + Constant.NL);
                                      print("error: invalid command");
                                      break;
                    }
                    //toServer.writeBytes(command + "\n");
                }
                print("AFTER");
                exit(toServer, fromServer, userInput, clientSocket); 

            } catch (UnknownHostException e) {
                System.err.println("Trying to connect to unknown host: " + e);
            } catch (IOException e) {
                System.err.println("IOException:  " + e);
            }
        }
    }

    public static void exit(DataOutputStream toServer, BufferedReader fromServer, BufferedReader userInput, Socket clientSocket) throws Exception {
        toServer.close();
        fromServer.close();
        userInput.close();
        clientSocket.close();
        System.exit(1); 
    }

    public static void print(Object o) {
        System.out.println(o);
    }

}
